<?php
  include("conn.php");
  date_default_timezone_set("Asia/Hong_kong");

  $fileinfo = $_FILES['up_file'];
  $path="../待审核文件/".$_FILES["up_file"]["name"];
  $name=$_FILES['up_file']['name'];
  $address=$path;

  $year=$_POST['year'];
  $subject=$_POST['subject'];
  $date=date('Y-m-d H:i:s');
  $nowYear=date('Y');
  $type="资料";
  $profession=$_POST['profession'];

  if($fileinfo==null || $year==null || $subject==null || $date==null || $profession==null){
    echo "<script type='text/javascript'>alert('资料上传失败，请补全信息');location='../uploadFile.html';</script>";
  }else{
      if ($_FILES['up_file']['size']==0) {
      echo "<script type='text/javascript'>alert('资料上传失败，文件过大，请重新选择文件');location='../uploadFile.html';</script>";
      }
      else{
        if($year>$nowYear-5 && $year<=$nowYear){
          $stmt=$db->prepare('insert into check0(name,year,uploadtime,subject,address,type,profession) values(?,?,?,?,?,?,?)');
          $stmt->execute(array($name,$year,$date,$subject,$address,$type,$profession));

          move_uploaded_file($fileinfo['tmp_name'],$path);    //上传文件

          echo "<script type='text/javascript'>alert('资料上传成功，返回主页');location='../userIndex.html';</script>";
        }else{
          echo "<script type='text/javascript'>alert('资料时间错误，请重新填写');location='../uploadFile.html';</script>";
        }
        
      }
  }

  

?>