# group_beta

#### 仓库目的

云复习资料库项目β阶段冲刺进度存储、备份

#### 项目介绍

是基于PHP、HTML5所写的一个，旨在为了帮助同学们在期末复习、课余时间来
寻找、搜罗各课资料、笔记、练习及一些老师偷偷透露的小道消息，让同学能够更
好地通过期末考试的网站。

#### 项目地址：

http://121.5.53.194

#### 内置功能介绍

1.  上传文件
2. 上传小道消息
3. 预览文件
4.下载文件
5.搜索文件
6.按选择条件对文件进行筛选、排序

#### 所使用的技术、框架

前端：HTML5、JQueryAjax
后端：PHP
数据库：MySQL
工具：IIS服务器部署、Wampserver

#### 演示图

![](https://images.gitee.com/uploads/images/2020/1221/185838_34101a83_7991926.png "屏幕截图.png")
![](https://images.gitee.com/uploads/images/2020/1221/185902_ae93ef2c_7991926.png "屏幕截图.png")
![](https://images.gitee.com/uploads/images/2020/1221/185913_a3a7d99a_7991926.png "屏幕截图.png")
![](https://images.gitee.com/uploads/images/2020/1221/185930_474ade36_7991926.png "屏幕截图.png")
![](https://images.gitee.com/uploads/images/2020/1221/185947_75627cbe_7991926.png "屏幕截图.png")
![](https://images.gitee.com/uploads/images/2020/1221/190004_ef97b249_7991926.png "屏幕截图.png")
![](https://images.gitee.com/uploads/images/2020/1221/190047_0648a638_7991926.png "屏幕截图.png")

#### 其它各类文档存储仓库地址

https://gitee.com/houdini/group

#### 参与贡献

1.  Fork 本仓库
2.  新建 xxx 分支
3.  提交代码
4.  新建 Pull Request
